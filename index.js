let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

console.log("Start of activity");

// Part 1

function addWrestler(wrestler){
    users.push(wrestler);

}
addWrestler("John Cena");
console.log(users);





// Part 2


let itemFound;

function getWrestler(wrestler2){
    itemFound = users[wrestler2];
        return itemFound;
}
itemFound = getWrestler(2);
console.log(itemFound)



// Part 3

let deletedWrestler;

function removeWrestler(lastWrestler){
     lastWrestler = users[users.length-1];
        users.length = users.length - 1;
        return lastWrestler;
        
}
deletedWrestler = removeWrestler();
console.log(deletedWrestler);
console.log(users);



// Part 4


function addWrestlerByIndex(index, wrestlerUpdate){
        let changeValue = users.splice(index, 1 , wrestlerUpdate);
        console.log(users);

}
addWrestlerByIndex(3, "triple H");



// Part 5


function toDelete(length){
    users.length = length;
}

toDelete(0);
console.log(users);


// Part 6

let isUsersEmpty;

function toCheck(){
    if (users.length > 0){

        isUsersEmpty = false;

    }

    else{
        isUsersEmpty = true;
    }
}

toCheck();
console.log(isUsersEmpty);
